/**
 * Created by Juan Pablo Capdevila on 11/12/16.
 */
"use strict";
let PredixTimeSeriesClient = new require('../index.js');

/*
 * Config Object Properties:
 * - uaaUrl - String
 * - clientId - String
 * - clientSecret - String
 * - ingestUrl - String
 * - queryUrl - String
 * - predixZoneId - String
 * - websocketOriginUrl - String
 */

const config = {
    uaaUrl: 'https://tombot.predix-uaa.run.aws-usw02-pr.ice.predix.io/oauth/token',
    clientId: 'tombot',
    clientSecret: 'tombot',
    ingestUrl: 'wss://gateway-predix-data-services.run.aws-usw02-pr.ice.predix.io/v1/stream/messages',
    apiBaseUrl: 'https://time-series-store-predix.run.aws-usw02-pr.ice.predix.io/v1',
    predixZoneId: '872578b1-e12a-4ba7-a542-c75f1fee7126',
    websocketOriginUrl: 'https://tombot.run.aws-usw02-pr.ice.predix.io/'
};
const config2 = {
    uaaUrl: 'https://98d4176d-a268-405b-a461-3f9944793a31.predix-uaa.run.aws-usw02-pr.ice.predix.io/oauth/token',
    clientId: 'app_client_id',
    clientSecret: 'secret',
    ingestUrl: 'wss://gateway-predix-data-services.run.aws-usw02-pr.ice.predix.io/v1/stream/messages',
    apiBaseUrl: 'https://time-series-store-predix.run.aws-usw02-pr.ice.predix.io/v1',
    predixZoneId: 'e3fba85e-d334-409e-87ce-3a17e71b4946',
    websocketOriginUrl: 'https://tombot.run.aws-usw02-pr.ice.predix.io/'
};

let predixTimeSeriesClient = new PredixTimeSeriesClient(config);

//predixTimeSeriesClient.openIngestWebsocket(function(ws){
//
//});

//predixTimeSeriesClient.getTags(console.log);


//Last temperature
predixTimeSeriesClient.getDatapoints(
    {
        "start": "1mi-ago",
        "tags": [
            {
                "name": "MM16_TEMP",
                "order": "desc",
                "limit": 1
            }
        ]
    },
    function (error, data) {
        console.log('Last', data.tags[0].results[0].values[0][1]);
    }
);

//Average 5 minutes
predixTimeSeriesClient.getDatapoints(
    {
        "start": "5mi-ago",
        "tags": [
            {
                "name": "MM16_TEMP",
                "aggregations": [
                    {
                        "type": "avg",
                        "interval": "5mi"
                    }
                ]
            }
        ]
    },
    function (error, data) {
        console.log('Avg', data.tags[0].results[0].values[0][1]);
    }
);

//Max last 5 minutes
predixTimeSeriesClient.getDatapoints(
    {
        "start": "5mi-ago",
        "tags": [
            {
                "name": "MM16_TEMP",
                "aggregations": [
                    {
                        "type": "max",
                        "interval": "5mi"
                    }
                ]
            }
        ]
    },
    function (error, data) {
        console.log('Max', data.tags[0].results[0].values[0][1]);
    }
);

//Min last 5 minutes
predixTimeSeriesClient.getDatapoints(
    {
        "start": "5mi-ago",
        "tags": [
            {
                "name": "MM16_TEMP",
                "aggregations": [
                    {
                        "type": "min",
                        "interval": "5mi"
                    }
                ]
            }
        ]
    },
    function (error, data) {
        console.log('Min', data.tags[0].results[0].values[0][1]);
    }
);

//Points of the last 5 minutes grouped by 10 seconds
predixTimeSeriesClient.getDatapoints(
    {
        "start": "5mi-ago",
        "tags": [
            {
                "name": "MM16_TEMP",
                "aggregations": [
                    {
                        "type": "min",
                        "interval": "30s"
                    }
                ]
            }
        ]
    },
    function (error, data) {

        //https://image-charts.com/chart?cht=lc&chs=700x125&chd=t:40,60,60,45,47,75,70,72
        var tempsString = '';
        for (var i = 0; i < data.tags[0].results[0].values.length; i++) {
            tempsString+= data.tags[0].results[0].values[i][1];
            if(i!=data.tags[0].results[0].values.length-1){
                tempsString+=','
            }
        }
        console.log('Chart', 'https://image-charts.com/chart?cht=lc&chs=700x125&chd=t:'+tempsString);

    }
);


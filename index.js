/**
 * Created by Juan Pablo Capdevila on 11/12/16.
 */
"use strict";

let EventEmitter = require('events');
let WebSocket = require('ws');
let uaaClient = require('predix-uaa-client');
let request = require('request');

const apiDatapointsPath = '/datapoints';
const apiTagsPath = '/tags';

/*
 * Config Object Properties:
 * - uaaUrl - String
 * - clientId - String
 * - clientSecret - String
 * - ingestUrl - String
 * - apiBaseUrl - String
 * - predixZoneId - String
 * - websocketOriginUrl - String
 */
module.exports = class PredixTimeseriesClient extends EventEmitter {
    constructor(config) {
        super();
        this.config = config;
        this.config.datapointsUrl = this.config.apiBaseUrl + apiDatapointsPath;
        this.config.tagsUrl = this.config.apiBaseUrl + apiTagsPath;
    }

    getToken(callback) {
        uaaClient.getToken(this.config.uaaUrl, this.config.clientId, this.config.clientSecret).then((token) => {
            this.token = token;
            callback(token);
        }).catch((err) => {
            console.error('Error getting token', err);
        });
    }

    openIngestWebsocket(callback) {
        this.getToken(()=> {
            console.log('Opening websocket');
            this.ws = new WebSocket(this.config.ingestUrl, {
                headers: this.getHeaders()
            });

            this.ws.on('open', () => {
                console.log('Websocket Opened');
                callback(this.ws);
            });
        });
    }

    sendDatapoit (data){
        this.ws.send(JSON.stringify({
            "messageId": Date.now(),
            "body": [
                data
            ]
        }));
    }

    getHeaders() {
        return {
            'authorization': 'Bearer ' + this.token.access_token,
            'predix-zone-id': this.config.predixZoneId,
            'Origin': this.config.websocketOriginUrl,
            'Content-Type': 'application/json'
        }
    }

    getTags(callback) {
        this.getToken(()=> {
            let options = {
                url: this.config.tagsUrl,
                json: true,
                headers: this.getHeaders(),
            };

            request(options, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    callback(null, body.results);
                } else {
                    callback(error || body);
                }
            });
        });
    }

    getDatapoints(query, callback) {
        this.getToken(()=> {
            let options = {
                url: this.config.datapointsUrl,
                json: true,
                method: 'POST',
                body: query,
                headers: this.getHeaders()
            };

            request(options, function (error, response, body) {
                //console.log(error,response,body);
                if (!error && response.statusCode == 200) {
                    //console.log(JSON.stringify(body));
                    var tags = body;
                    callback(null, tags);
                } else {
                    callback(error || body);
                }
            });
        })
    }
};
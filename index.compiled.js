/**
 * Created by Juan Pablo Capdevila on 11/12/16.
 */
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var EventEmitter = require('events');
var WebSocket = require('ws');
var uaaClient = require('predix-uaa-client');
var request = require('request');

var apiDatapointsPath = '/datapoints';
var apiTagsPath = '/tags';

/*
 * Config Object Properties:
 * - uaaUrl - String
 * - clientId - String
 * - clientSecret - String
 * - ingestUrl - String
 * - apiBaseUrl - String
 * - predixZoneId - String
 * - websocketOriginUrl - String
 */
module.exports = function (_EventEmitter) {
    _inherits(PredixTimeseriesClient, _EventEmitter);

    function PredixTimeseriesClient(config) {
        _classCallCheck(this, PredixTimeseriesClient);

        var _this = _possibleConstructorReturn(this, (PredixTimeseriesClient.__proto__ || Object.getPrototypeOf(PredixTimeseriesClient)).call(this));

        _this.config = config;
        _this.config.datapointsUrl = _this.config.apiBaseUrl + apiDatapointsPath;
        _this.config.tagsUrl = _this.config.apiBaseUrl + apiTagsPath;
        return _this;
    }

    _createClass(PredixTimeseriesClient, [{
        key: 'getToken',
        value: function getToken(callback) {
            var _this2 = this;

            uaaClient.getToken(this.config.uaaUrl, this.config.clientId, this.config.clientSecret).then(function (token) {
                _this2.token = token;
                callback(token);
            }).catch(function (err) {
                console.error('Error getting token', err);
            });
        }
    }, {
        key: 'openIngestWebsocket',
        value: function openIngestWebsocket(callback) {
            var _this3 = this;

            this.getToken(function () {
                console.log('Opening websocket');
                _this3.ws = new WebSocket(_this3.config.ingestUrl, {
                    headers: _this3.getHeaders()
                });

                _this3.ws.on('open', function () {
                    console.log('Websocket Opened');
                    callback(_this3.ws);
                });
            });
        }
    }, {
        key: 'sendDatapoit',
        value: function sendDatapoit(data) {
            this.ws.send(JSON.stringify({
                "messageId": Date.now(),
                "body": [data]
            }));
        }
    }, {
        key: 'getHeaders',
        value: function getHeaders() {
            return {
                'authorization': 'Bearer ' + this.token.access_token,
                'predix-zone-id': this.config.predixZoneId,
                'Origin': this.config.websocketOriginUrl,
                'Content-Type': 'application/json'
            };
        }
    }, {
        key: 'getTags',
        value: function getTags(callback) {
            var _this4 = this;

            this.getToken(function () {
                var options = {
                    url: _this4.config.tagsUrl,
                    json: true,
                    headers: _this4.getHeaders()
                };

                request(options, function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        callback(null, body.results);
                    } else {
                        callback(error || body);
                    }
                });
            });
        }
    }, {
        key: 'getDatapoints',
        value: function getDatapoints(query, callback) {
            var options = {
                url: this.config.tagsUrl,
                json: true,
                method: 'POST',
                body: query,
                headers: this.getHeaders()
            };

            request(options, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    var tags = JSON.parse(body).results;
                    callback(null, tags);
                } else {
                    callback(error || body);
                }
            });
        }
    }]);

    return PredixTimeseriesClient;
}(EventEmitter);